cmake_minimum_required(VERSION 2.8)

include(${CTEST_SCRIPT_DIRECTORY}/../lcgjenkins/common.cmake)

set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_COMMAND "make -k -j${ncpu} $ENV{TARGET}")
set(CTEST_BUILD_NAME $ENV{LCG_VERSION}-$ENV{PLATFORM})

if(DEFINED ENV{BUILDHOSTNAME})
    set(CTEST_SITE $ENV{BUILDHOSTNAME})
else()
    set(CTEST_SITE ${host})
endif()

set(CTEST_BUILD_PREFIX "$ENV{WORKSPACE}")
set(CTEST_SOURCE_DIRECTORY "${CTEST_BUILD_PREFIX}/lcg-csh-test")
set(CTEST_BINARY_DIRECTORY "${CTEST_BUILD_PREFIX}/test_build")
set(CTEST_INSTALL_DIRECTORY "${CTEST_BUILD_PREFIX}/install")

set(options -DCMAKE_INSTALL_PREFIX=${CTEST_INSTALL_DIRECTORY}
            -DCMAKE_VERBOSE_MAKEFILE=OFF
            $ENV{LCG_EXTRA_OPTIONS})

ctest_start($ENV{MODE} TRACK $ENV{MODE} APPEND)
ctest_configure(BUILD   ${CTEST_BINARY_DIRECTORY}
                SOURCE  ${CTEST_SOURCE_DIRECTORY}
                OPTIONS "${options}" APPEND)
ctest_test(APPEND PARALLEL_LEVEL ${ncpu} INCLUDE_LABEL "${CTEST_LABELS}"
           RETURN_VALUE rc)
ctest_submit(PARTS Test)
if(NOT rc EQUAL 0)
   message(FATAL_ERROR "One or more tests have failed (rc = ${rc})")
endif()
