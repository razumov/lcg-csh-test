#!/bin/bash -e
SHEL=$1;shift;
VIEW=$1;shift;
if [ $SHEL == 'bash' ]; then
  bash -xev $WORKSPACE/lcg-csh-test/runme.sh "$VIEW/setup.sh"
else
  if [ $SHEL == 'zsh' ]; then
    zsh -xev $WORKSPACE/lcg-csh-test/runme.sh "$VIEW/setup.sh"
  else
    # '-f' disables init scripts
    tcsh -xevf $WORKSPACE/lcg-csh-test/runme.csh "$VIEW/setup.csh"
  fi
fi
